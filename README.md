# UConn Stat1100 Fall 2021

This is the Git repository for the STAT1100 course taught at UConn's
Hartford campus in fall 2021. 

This repository contains copies of the syllabus,
slides, and exam preparatory material. It also contains helpful
supplemental information, like tutorials, visualizations, and
statistical simulations.

## Contents

## Outside Supplemental Resources

### Computing

#### R

R is a programming language primarily used for statistical
computing. R began as an open source alternative to an older language
called S, a proprietary implementation of which called S-PLUS gained
popularity for statistical computing in the 1990s.

##### Resources to learn R

#### Python

Python is a general purpose programming language used in a variety of
applications.

##### Resources to learn Python

#### Git

Git is a *version control* tool widely used in software development,
both in industry and academia.

##### What is *version control*? 

Have you ever been working on something, especially in collaboration
with others, and found yourself naming files like: Paper_v1.doc,
Paper_v2.doc, Paper_final.doc, Paper_finalreally.doc,
Paper_absolutelyfinal.doc, etc.?

In addition to wanting to keep snapshots of your work and be sure you
were working on the most up-to-date version, you probably also wanted
to do other things like:
- Record comments about what changed between two versions.
- Allow different people to work on different parts of a project at
  the same time, without causing conflicts.
- Assign specific pieces of work to specific members of the group.
- Maintain a centralized, canonical version of the project that could
  not be lost or accidentally deleted.
  
These requirements and more are taken care of by version control
systems. Version control really shines in software development, but
can be adapted to any digital work (e.g. writing papers, graphic
design, video editing, etc.).

##### What is Git?

Git is the current industry standard tool for version
control. Introduced in 2005, Git is associated with the Linux project,
and was developed with large, geographically-distributed teams in
mind. As such, individual users and small teams often use only a
subset of Git's functionality.

Version control existed long before 2005; early version control
programs were distributed with IBM machines in the 1970s, and file
version snapshotting was built into some operating systems in the
1960s. Popular precursors to Git (still used in some places today) are
Apache Subversion (aka SVN, released in 2000) and Concurrent Versions
System (aka CVS, released in 1990). The primary contemporary
alternative to Git is a similar system called Mercurial; the two
differ in some technical details.

##### Basics of Git

A Git /commit/ is a snapshot of all the tracked files in a project at
a given moment. Commits are available in perpetuity, so that a
developer can always reference the state of the project at some points
in the past no matter how much time or work has passed.

##### Resources to learn Git

Git's learning curve is not very steep; daily use for most people
consists of just a few commands. However it starts with one major
assumption: that you are working in an environment where you can
install and interact with the tool. Many tutorials assume that a user
is working on the command line on a MacOS or Linux machine, where
```git``` is literally available as a command line utility with no
setup required. New users who don't fit this profile can become stuck
immediately, at the "what am I even looking at here?" step.

### History of Statistics


### Some Current Applications of Statistics


### Statistics Organizations and Societies

