\documentclass[12pt]{article}
\usepackage{fullpage, graphicx, amsmath}

\begin{document}

\begin{center}
TI-83/84 Tutorial for Exam 2
\end{center}

This document outlines the main functions on the TI-83/84 calculators
needed for the second unit of the course.

\begin{flushleft}
\underline{Random sampling}
\end{flushleft}

\begin{itemize}

\item[-] \textbf{randInt}: selects random integers between two
  endpoints, including the endpoints as candidates.

  \underline{Location}:MATH$\to$PRB$\to$5

  \underline{Argument structure}: randInt takes three arguments: $a$,
  $b$, $n$. $a$ is the smallest integer allowable in the sample, and
  $b$ is the largest. $n$ is the number of random integers that you
  want.

  \underline{Example}: suppose we want to carry out a simple random
  sample of size 10 on a population of size 100. Each individual is
  first numbered from 1 to 100. In randInt, our $a$ is 1, and our $b$
  is 100. We want 10 unique numbers; since randInt samples
  \underline{with} replacement, we should make $n$ slightly more than
  10 to ensure that we get 10 unique numbers. Usually requesting $1
  \frac{1}{2}$ as many numbers as you actually need is enough
  insurance. 

  Also, only about five two-digit numbers can print to the screen
  before they scroll off. When generating a list of random numbers,
  store them to one of the list variables so that you can view them
  all.

  We can do this by typing $\textbf{randInt}(1, 100, 15) \mbox{STO }
  \mbox{L1}$ (the storage button is right above the on button). It's
  important to close the parentheses on randInt; otherwise you might
  get an error or an undesired result. There are no spaces in the
  above syntax; after closing randInt, press the storage button and L1
   without anything between them. If you look in L1, your random
   sample will be stored.

\end{itemize}

\begin{flushleft}
\underline{Basic probability}
\end{flushleft}

\begin{itemize}

\item[-] \textbf{nCr}: calculates $\binom{n}{r}$, the number of ways
  that $r$ objects can be chosen from a group of $n$ objects.

  \underline{Location}: Math$\to$PRB$\to$3.

  \underline{Argument structure}: unlike most of the functions on the
  TI calculators, the arguments of nCr don't all follow the function
  name. Instead, n comes first, then nCr, then r. Press enter to
  evaluate. Note that you don't need to manually put spaces between
  the arguments and the function name, this is done automatically.

  \underline{Example}: Suppose a machine has 15 different gears, which
  sometimes break. How many different ways can 4 gears out of the 15
  break?\\
  We can think of the solution as ``choosing'' four gears to be broken
  out of the 15, and calculating how many different ways there are of
  making this choice. The answer is $\binom{15}{4}$, which we can do
  on the calculator as 15 nCr 4. The result is 1365.
  

\end{itemize}

\begin{flushleft}
\underline{Random variables}
\end{flushleft}

\begin{itemize}

\item[-] \textbf{Expected value}: $E[X] = \sum_{k} P(X = k)k$,
  i.e. each potential value of the random variable times its
  probability.

  To avoid computing this by hand, you can enter the values and
  probabilities into the list editor. Create a third column as the
  product of the value and probability columns. Then, use 1-Var-Stats
  in the STAT$\to$CALC menu to get the sum.

  \underline{Example}: a lottery has the following payout structure:
  there is a 90\% chance of winning nothing, a 3\% chance of winning
  \$10, a 2\% chance of winning \$20, and a 1\% chance each of winning
  \$50, \$75, and \$100. A ticket for this lottery costs \$3.50. You
  have the option of playing this lottery as many times as you want,
  so you decide that a good measure of how much you expect to win is
  the long-term average (i.e. the expected value). On average, will
  you make or lose money playing this lottery?

  First, enter the vector 0.9, 0.03, 0.02, 0.01, 0.01, 0.01 into L1 in
  the list editor. Then, enter the vector 0, 10, 20, 50, 75, 100 into
  L2. Into the column header of L3, type L1 * L2, and press enter;
  this will populate L3 with the product of the values and their
  probabilities. Next, run 1-Var-Stats on L3; among other things, this
  tells us that the sum of L3 (the expected value of $X$) is
  \$2.95. So on average, you expect to win \$2.95 from a ticket for
  this lottery, which costs \$3.50. So on average, you are losing
  \$0.55 every time you play this lottery.

\item[-] \textbf{Standard deviation}: $\sigma = \sqrt{\sum (k -
    E[X])^2 P(X = k)}$, i.e. the square root of the sum of the squared
  difference between each value and the expected value, times each
  value's probability.

  As in the case of the expected value, it's easier to use the
  calculator to compute this than to work it out by hand. We start off
  as in the previous case, with the probabilities entered into L1 and
  the values entered into L2. The expected value shows up in the
  formula for $\sigma$, so we assume that we have already calculated
  this. Create L3 as $(\mbox{L2} - E[X])^2 * \mbox{L1}$, and run
  1-Var-Stats to get its sum. What you have now is $\sigma^2$, so as
  the last step, find the square root of this sum.

  \underline{Example}: let's find the standard deviation of the
  lottery from the previous example. We enter the column header of L3,
  and type $(\mbox{L2} - 2.95)^2 * \mbox{L1}$, and take the
  1-Var-Stats of L3 to get that $\sigma^2 = 183.37345$. Taking the
  square root of this gives us $\sigma \approx 13.54$, meaning that
  this lottery has a standard deviation of \$13.54.

\end{itemize}

\begin{flushleft}
\underline{Binomial distribution}
\end{flushleft}

\begin{itemize}

\item[-] \textbf{binompdf}: calculates the probability of a
  Binomial$(n,p)$ variable taking the value $k$, where $k$ is an
  integer.\\ 

  \underline{Location}: 2nd$\to$VARS$\to$A
  
  \underline{Argument structure}: $\mbox{binompdf}(n,p,k)$. $n$ is the
  number of trials, $p$ is the probability of success in a single
  constituent trial, and $k$ is the value whose probability we want to
  calculate.

  \underline{Example}: I roll a six-sided die 15 times. I want to
  know the probability that the number 3 is facing up exactly 8 times
  out of those 15 rolls. Since the probability that an individual roll
  comes up 3 is $\frac{1}{6}$, $p=\frac{1}{6}$. The number of trials
  is the number of total rolls, so $n=15$. Finally, I want the
  probability that the event (i.e. the die showing a 3 after being
  rolled) occurs exactly 8 times. So the solution to this problem is
  $\mbox{binompdf}(15,\frac{1}{6},8)\approx0.00106923$.

\item[-] \textbf{binomcdf}: calculates the probability of a
  Binomial$(n,p)$ variable taking a value less than or equal to $k$,
  where $k$ is an integer.

  \underline{Location}: 2nd$\to$VARS$\to$B

  \underline{Argument structure}: $\mbox{binompdf}(n,p,k)$. $n$ is the
  number of trials, $p$ is the probability of success in a single
  constituent trial, and $k$ is an integer that specifies the $[0,k]$
  interval that the probability will be calculated on.

  Example: 80 voters are deciding on a ballot measure, which will pass
  if a proper majority (i.e. $>40$ voters) support it. An opinion poll
  revealed that each voter has a 49\% probability of supporting the
  ballot measure. We assume that the voters vote independently, and
  want to know the probability that the measure will pass. Since the
  CDF function counts from the left, instead of calculating the
  probability of passage, it's easier to calculate the probability of
  failure and subtract that from 1. The ballot measure will fail if 40
  or fewer voters vote for it. So the probability that the measure
  will fail is $\mbox{binomcdf}(80,.49,40)\approx 0.6145321$. Then the
  probability that the measure passes is $1-0.6145321 = 0.3854679$.

\end{itemize}

\end{document}