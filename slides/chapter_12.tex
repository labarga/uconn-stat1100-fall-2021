\documentclass[size=screen,xcolor=table]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,multirow,mathtools}
\begin{document}
\title{Chapter 12: Hypothesis Testing for the Population Proportion}
\author{Instructor: John Labarga}
\date{November 30, 2021}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Motivation for Hypothesis Testing}

\begin{frame}
  \frametitle{Evaluating Statements About Parameters}

  So far, we have taken statements like ``this coin is fair'' as given
  when working with population distributions.

  \vspace{3mm}

  However, we have sometimes broached the possibility that empirical
  evidence (e.g. sample statistics) might cause us to doubt whether
  these statements about population parameters are true (e.g. if we
  were told ``this coin is fair'', then watched it return 10 heads out
  of 10 flips).

  \vspace{3mm}

  Hypothesis testing, the second inference tool we will learn, allows
  us to assess the plausibility of statements about the population in
  light of empirical evidence.

\end{frame}

\begin{frame}
  \frametitle{Example}

  Suppose you are given a coin and told the coin is fair. Instead of
  taking this as a given fact, you flip the coin 100 times to
  ascertain whether it really is fair.

  \vspace{3mm}

  You know that the number of heads that come up, \emph{if the coin
    really is fair}, is a $\mbox{Binomial}(100, 0.5)$ variable. As
  such, a fair coin can generate outcomes other than a 50/50 split.

  \vspace{3mm}

  If you saw 55 heads and 45 tails, you would probably conclude that
  the coin likely is fair.

  \vspace{3mm}

  If you saw 70 heads and 30 tails, however, you might start to
  suspect that the coin is biased towards heads, and not fair.

\end{frame}


\section{Logic and Framework for Hypothesis Testing}

\subsection{Introduction}

\begin{frame}
  \frametitle{Hypotheses}

  The ``hypotheses'' that we're learning to test are statements about
  population parameters. Examples:

  \begin{itemize}

    \item The probability of this coin coming up heads ($p$) is 0.5\\
      Hypothesis: $p = 0.5$

    \item The proportion of voters who support candidate A over
      candidate B ($p$) is more than 60\%\\ Hypothesis: $p > 0.6$

    \end{itemize}

    \vspace{5mm}

    Hypothesis testing is a set of procedures for evaluating empirical
    evidence against these statements, and determining whether the
    empirical evidence supports them.
  
  \end{frame}

\subsection{Setup}

\begin{frame}
  \frametitle{Structuring a Hypothesis Testing Problem}

  Hypothesis testing typically arises when a researcher wants to
  demonstrate that the population parameter likely has some property
  (e.g. is equal to some amount, or more/less than that amount).

  \vspace{3mm}

  Usually, this property is different from the conventional wisdom or
  currently accepted knowledge about the parameter, which can be
  regarded as the ``base case''.

  \vspace{3mm}

  This leads to a situation where the researcher needs to
  differentiate their hypothesis from the current accepted knowledge
  about the population parameter, and show that the empirical evidence
  strongly favors their hypothesis.
  
\end{frame}

\begin{frame}
  \frametitle{Null and Alternative Hypotheses}

  We formalize this situation as follows:

  \begin{itemize}

    \item The accepted knowledge about the population
      parameter is called the \textbf{null hypothesis}. This is the
      current accepted ``base case'', which the researcher is trying
      to show is not correct.


    \item The researcher is instead trying to demonstrate that their
      hypothesis, called the \textbf{alternative hypothesis}, is true.

    \end{itemize}

    \vspace{3mm}

    The null and alternative hypotheses cannot have any values of the
    population parameter in common, i.e. they can't simultaneously be
    true.
  
  \end{frame}

\begin{frame}
  \frametitle{Notation}

  Hypotheses:

  \begin{itemize}

  \item Null hypothesis: $H_0$

  \item Alternative hypothesis: $H_1$

  \end{itemize}

  \vspace{3mm}

  Types of hypothesis tests:

  \begin{itemize}

  \item In a one-tailed hypothesis test, we have either:

    \begin{itemize}

      \item $H_0: p \leq p_0$ and $H_1: p > p_0$, or

      \item $H_0: p \geq p_0$ and $H_1: p < p_0$

        
    \end{itemize}
    
  \item In a two-tailed hypothesis test, we have $H_0: p = p_0$ and $H_1: p \neq p_0$.

  \end{itemize}
  
  \end{frame}

\begin{frame}
  \frametitle{Recognizing Null and Alternative Hypotheses}

  \begin{enumerate}

  \item A pharmaceutical company claims that fewer than 20\% of its
    patients experience side effects from a given medication.\\
    $H_0: p \geq 0.2$\\
    $H_1: p < 0.2$\\
    This is a one-tailed hypothesis test


  \item A coin is claimed to be fair (i.e. comes up heads or tails
    with equal probability); you doubt this and want to show otherwise.\\
    $H_0: p = 0.5$\\
    $H_1: p \neq 0.5$\\
    This is a two-tailed hypothesis test

    \end{enumerate}
  
  \end{frame}

  \subsection{Components of a Hypothesis Test}

  \begin{frame}

    We test a hypothesis by identifying a \textbf{test statistic}: a
    transformed version of a sample statistic (usually a z-score)
    whose distribution we know \emph{if the null hypothesis is
      true}. Usually we build the test statistic so that is has a
    $N(0,1)$ distribution if the null hypothesis is true:

    $$\mbox{test statistic} = \frac{\mbox{sample statistic} - \mbox{null
        value}}{\mbox{null standard error}}$$

    \vspace{3mm}

    From the test statistic we calculate a \textbf{p-value}: the
    probability that random sampling could generate a test statistic
    as extreme or more extreme (in the direction of the alternative
    hypothesis) as the test statistic observed in our sample,
    \textbf{if the null hypothesis is true}.

    \vspace{3mm}

    We compare the p-value to a \textbf{significance level}: a
    threshold value such that, if we see a p-value below this
    threshold value, we will conclude that the null hypothesis is
    probably not true.
  
\end{frame}

\begin{frame}
  \frametitle{Reaching a Conclusion in a Hypothesis Test}

    The hypothesis testing procedure result in the p-value, which can
    be compared to the significance level (also called the $\alpha$
    level) to reach a conclusion.

    \vspace{3mm}

    If the p-value is below the $\alpha$ level, then the experiment
    has generated data that is so inconsistent with the null
    hypothesis that we \emph{reject} the null hypothesis in favor of
    the alternative hypothesis.

    \vspace{3mm}

    Otherwise, we \emph{fail to reject} the null hypothesis (we never
    ``accept'' the null hypothesis).

    \vspace{3mm}

    The smaller the p-value, the stronger the evidence in favor of
    the alternative hypothesis.
  
\end{frame}

\begin{frame}
  \frametitle{A Legal Analogy}

  In a criminal jury trial under the US legal system, a defendant is
  presumed innocent until proven guilty:

  \begin{itemize}

  \item Null hypothesis: innocent

  \item Alternative hypothesis: guilty

  \end{itemize}

  \vspace{3mm}

  At the end of the trial, one of two conclusions will be reached:

  \begin{itemize}

  \item Reject the null hypothesis: convict

  \item Fail to reject the null hypothesis: exhonerate

  \end{itemize}

  
  \vspace{3mm}

  The job of the prosecutor is to build evidence toward the
  alternative hypothesis, and the job of the defense is to draw the
  jury back towards the null hypothesis.

\end{frame}

\begin{frame}
  \frametitle{A Legal Analogy}

  The trial builds a test statistic (the weight of the evidence) in
  the mind of each juror. Each juror decides whether to convict based
  on whether that test statistic is so large that the person is guilty
  ``beyond a reasonable doubt''.

  \vspace{3mm}

  In a jury trial, the p-value is the probability that the prosecutor
  could build a case as strong as, or stronger than, the one presented
  in this trial \emph{on an innocent person}. The jury should only
  convict if this probability is very low.

\end{frame}

\begin{frame}
  \frametitle{A Legal Analogy}

  Since a person can be either innocent or guilty, and the jury can
  either convict or exhonerate them, four outcomes are possible:

  \begin{table}[]
    \begin{tabular}{|c|c|c|}
      \hline
      & Innocent         & Guilty           \\ \hline
      Convict    & Type 1 Error     & Correct Decision \\ \hline
      Exhonerate & Correct Decision & Type 2 Error     \\ \hline
    \end{tabular}
  \end{table}

  \vspace{3mm}

  Common-law legal systems tend to regard the type 1 error as more
  serious than the type 2 error.

  \vspace{3mm}

  \begin{quote}
    It is better that ten guilty persons escape than that one innocent
    suffer. - Blackstone, 18th Century English Jurist.
  \end{quote}

\end{frame}

\begin{frame}
  \frametitle{Types of Errors}

  In hypothesis testing the same errors are possible:

  \begin{table}[]
    \begin{tabular}{|c|c|c|}
      \hline
      & $H_0$ is True         & $H_0$ is False           \\ \hline
      Reject $H_0$    & Type 1 Error     & Correct Decision \\ \hline
      Fail to Reject $H_0$ & Correct Decision & Type 2 Error     \\ \hline
    \end{tabular}
  \end{table}

  \vspace{3mm}

  In most settings, we consider type 1 error to be more serious, and
  harder to correct than type 2 error.

  \vspace{3mm}

  In science, this is because if a phenomenon is true, then a single
  experiment failing to demonstrate it will be overturned later in
  further experiments. Whereas if something is falsely concluded, it
  will take a lot of work to reverse the error.

\end{frame}

\begin{frame}
  \frametitle{Relationship Between the p-value and Type 1 Error}

  Since the p-value is the probability that the null hypothesis could
  generate a test statistic at least as extreme as the one just
  observed, the p-value is the probability of committing a type 1
  error if we reject the null hypothesis.

  \vspace{3mm}

  We would like this to be low. In particular, since we will reject
  the null hypothesis if the p-value is below the $\alpha$ level, the
  $\alpha$ level can be interpreted as our tolerance for committing a
  type 1 error.

  \vspace{3mm}

  The $\alpha$ level used in a hypothesis test is contextual, and
  depends on a field's tolerance for type 1 error. In medicine it's
  often 0.001 or less, whereas in less critical applications it may be
  0.05 or 0.1.

\end{frame}

\section{Hypothesis Testing for $p$}

\begin{frame}
  \frametitle{Test Statistic for $\hat{p}$}

  Recall that the sampling distribution for $\hat{p}$ is:

  $$N \left( p, \sqrt{\frac{p(1-p)}{n}} \right)$$

  So if the null hypothesis claims that $p = p_0$, then the quantity:

  $$\frac{\hat{p} - p_0}{\sqrt{\frac{p_0(1-p_0)}{n}}}$$

  has a standard normal distribution, and can be used as a test
  statistic for a hypothesis test under the conditions of the central
  limit theorem.

\end{frame}

\begin{frame}
  \frametitle{Conditions for the z-Test}

  Since we are relying on the central limit theorem to give our test
  statistic a $N(0,1)$ distribution, we must meet its conditions:

  \begin{enumerate}

  \item $n \geq 30$

  \item $X \geq 10$

  \item $n-X \geq 10$

  \end{enumerate}

  \vspace{3mm}

  If these conditions are met, then the following calculator function
  can be used to conduct a hypothesis test:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{1-PropZTest}$$
  

\end{frame}

\begin{frame}
  \frametitle{Example}

  A drug company claims that one of its products causes side effects
  in less than 20\% of people. It wants to demonstrate this statement
  in a clinical trial with 400 participants.

  \vspace{3mm}

  In the clinical trial, 68 of the participants experience side
  effects. Conduct a hypothesis test of the drug company's claim with
  $\alpha = 0.01$.
  
\end{frame}

\begin{frame}
  \frametitle{Example}

  First, set up the hypotheses. Since the drug company wants to show
  that $p < 0.2$, this is the alternative hypothesis. Our hypotheses
  are:

  $H_0: p \geq 0.2$\\
  $H_1: p < 0.2$

  \vspace{3mm}

  The conditions to use the 1-PropZTest are met. Doing so yields:

  \begin{itemize}
  \item Test statistic: -1.5
  \item p-value: 0.0668
  \end{itemize}

  \vspace{3mm}

  \underline{Conclusion}
  
  Since the p-value was above the alpha level, we \textbf{fail to
    reject the null hypothesis}.
  
\end{frame}


\end{document}
