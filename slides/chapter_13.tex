\documentclass[size=screen,xcolor=table]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,multirow,mathtools}
\begin{document}
\title{Chapter 13: Hypothesis Testing for the Population Mean}
\author{Instructor: John Labarga}
\date{December 07, 2021}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Introduction}

\begin{frame}
  \frametitle{Differences and Similarities with the Case of $p$}

  As with confidence intervals, hypothesis testing for $\mu$ follows
  the same methodology as hypothesis testing for $p$:

  \begin{enumerate}

  \item Choose an $\alpha$-level.

  \item Formulate the hypotheses

  \item Calculate the test statistic and p-value.

  \item Compare the p-value to the alpha level

    \begin{itemize}

    \item If p-value $<$ $\alpha$-level, reject $H_0$

      \item Otherwise, fail to reject $H_0$

    \end{itemize}
    

  \end{enumerate}
  

  \vspace{3mm}

  There is an additional detail when building hypothesis tests for
  $\mu$ vs. $p$: whether $\sigma$ is known or unknown.

  \begin{itemize}

  \item $\sigma$ is known, population is normal $\rightarrow$ Z-Test

  \item Otherwise $\rightarrow$ T-Test (conditions must be met)
    
  \end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Structuring Hypotheses}

  The null and alternative hypotheses are still structured as opposing
  statements about the population parameter, in this case $\mu$. The
  hypotheses are commonly constructed as one of the following:

  \begin{itemize}

  \item $H_0: \mu = \mu_0$, $H_1: \mu \neq \mu_0$

  \item $H_0: \mu \leq \mu_0$, $H_1: \mu > \mu_0$

  \item $H_0: \mu \geq \mu_0$, $H_1: \mu < \mu_0$
    
  \end{itemize}  

  \vspace{3mm}

  However, in same cases not every possible value of $\mu$ is
  contained in one of the two hypotheses; the following are allowable
  hypothesis structures:

  \begin{itemize}

  \item $H_0: \mu = \mu_0$, $H_1: \mu > \mu_0$

  \item $H_0: \mu = \mu_0$, $H_1: \mu < \mu_0$
    
  \end{itemize}

  
\end{frame}

\section{Known $\sigma$}

\begin{frame}
  \frametitle{Constructing the Test Statistic}

  Suppose we know that the population distribution is normal, and
  furthermore we know the population value of $\sigma$. Then recall
  from chapter 09 that the sampling distribution of $\bar{x}$ is:

  $$N\left( \mu, \frac{\sigma}{\sqrt{n}}\right)$$

  Suppose $H_0$ is true. Then this sampling distribution is:

  $$N\left( \mu_0, \frac{\sigma}{\sqrt{n}}\right)$$
  
\end{frame}

\begin{frame}
  \frametitle{Constructing the Test Statistic}

  A consequence of this is that the quantity:

  $$z = \frac{\bar{x} - \mu_0}{\frac{\sigma}{\sqrt{n}}}$$

  is a test statistic with a $N(0,1)$ distribution. This test
  statistic can be used to conduct a hypothesis test called the
  \emph{Z-Test}, implemented on the calculator as:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{Z-Test}$$
  
\end{frame}

\section{Unknown $\sigma$}

\begin{frame}
  \frametitle{Constructing the Test Statistic}

  In applied settings, it's unlikely that the population value of
  $\sigma$ would be known. Instead, we would need to estimate $\sigma$
  with the sample standard deviation $s$, resulting in the
  \emph{standard error}:

  $$\frac{s}{\sqrt{n}}$$

  The corresponding test statistic, after substituting $s$ for
  $\sigma$, is:

  $$t = \frac{\bar{x} - \mu_0}{\frac{s}{\sqrt{n}}}$$
  
\end{frame}

\begin{frame}
  \frametitle{Constructing the Test Statistic}

  The quantity $t$ is a test statistic that has a $t$ distribution
  with $n-1$ degrees of freedom, if one of the following conditions is
  true:

  \begin{itemize}

    \item The sample is known to be drawn from a normal distribution
      of any size, \textbf{or}

    \item The sample is drawn from any distribution, but the sample
      size is at least 30.
      
    \end{itemize}

    If one of these conditions is met, we can conduct a T-Test. This
    is implemented on the calculator as:

    $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{T-Test}$$
  
  \end{frame}

\begin{frame}
  \frametitle{Example: Common Wisdom About Body Temperature}

  A researcher is trying to evaluate the common claim that mean human
  body temperature is equal to $98.6^\circ\mbox{F}$. The researcher
  believes that mean human body temperature is actually less than
  this.

  \vspace{3mm}

  To evaluate the claim, collects body temperature readings from 16
  people:

  $$[98.4, 98.6, 98.8, 98.8, 98.0, 97.9, 98.5, 97.6,$$

  $$98.4, 98.3, 98.9, 98.1, 97.3, 97.8, 98.4, 97.4]$$
  
\end{frame}

\begin{frame}
  \frametitle{Example: Common Wisdom About Body Temperature}

  Since the researcher wants to show that mean body temperature is
  actually less than 98.6, the hypotheses should be structured as:
  \begin{itemize}

  \item $H_0: \mu = 98.6$

  \item $H_1: \mu < 98.6$

  \end{itemize}

  \vspace{3mm}

  Conduct a hypothesis test of the researcher's claim at an
  $\alpha$-level of 0.01.
    
\end{frame}

\begin{frame}
  \frametitle{Example: Mean Tire Pressure in Cars}

  The recommended air pressure in passenger car tires is
  32PSI. Officials from a highway safety agency believe that the
  average car has low tire pressure, and want to carry out a
  hypothesis test to demonstrate this.

  \vspace{3mm}

  Data is collected by stopping 50 cars at a checkpoint, and measuring
  the pressure in each car's front left tire. The mean tire pressure
  in this sample was 30.1PSI, and with standard deviation of 3PSI.

  \vspace{3mm}

  Conduct a hypothesis test of the safety officials' claim at an
  $\alpha$-level of 0.001.
    
\end{frame}


\begin{frame}
  \frametitle{Example: Mean Tire Pressure in Cars}

  Since safety officials believe that mean tire pressure is
  actually less than 32PSI, the hypotheses should be structured as:
  \begin{itemize}

  \item $H_0: \mu \geq 32$

  \item $H_1: \mu < 32$

  \end{itemize}

  \vspace{3mm}

  We carry out the hypothesis test with:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{T-Test}$$
    
\end{frame}



\end{document}
