library(ggplot2)

# frequentist convergence examples

## generate 1000 samples from a fair coin, and one with P(H) = 0.6
fair_coin <- rbinom(10000, 1, 0.5)

biased_coin <- rbinom(10000, 1, 0.6)

get_partial_mean <- function(data, n){
  return(mean(data[1:n]))
}

partial_means <- data.frame(n = as.numeric(seq(1000, 10000, by=1000)))

partial_means$fair_coin <- as.numeric(lapply(partial_means$n, get_partial_mean, data=fair_coin))

partial_means$biased_coin <- as.numeric(lapply(partial_means$n, get_partial_mean, data=biased_coin))

png("../figures/ch07/01.png", width = 1000, height = 700)

ggplot(partial_means, aes(x=n, y=fair_coin)) + 
  geom_point(color="blue") + geom_line(color="blue") + 
  geom_point(data=partial_means, mapping=aes(x=n, y=biased_coin), color='red') + 
  geom_line(data=partial_means, mapping=aes(x=n, y=biased_coin), color='red') +
  geom_hline(yintercept=0.5, color="blue", linetype="dashed") +
  geom_hline(yintercept=0.6, color="red", linetype="dashed") +
  xlab("First n Coin Flips") +
  ylab("Proportion of Heads") +
  theme(axis.text=element_text(size=16), axis.title=element_text(size=18,face="bold"))
  
dev.off()
